﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace PracticalProject
{
    public partial class Autorization : Form
    {
        public Autorization()
        {
            InitializeComponent();
            textBox2.UseSystemPasswordChar = true;
          

        }
        string connection = ConnectSQL.Connect();
        SqlDataAdapter sqlDataAdapter;
        DataSet dataSet;
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Autorization_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
        Account account;
        string FIO;
        private void button1_Click(object sender, EventArgs e)
        {
            bool CheckedSignInRiel = false;
            bool CheckedSignInClient = false;
            using (SqlConnection sqlConnection = new SqlConnection(connection))
            {
                sqlDataAdapter = new SqlDataAdapter("select Номер_телефона, Email, Пароль, Фамилия, Имя, Отчество from Риэлторы where Статус_удаления = 'false'", sqlConnection);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    if (textBox1.Text == dataSet.Tables[0].Rows[i][0].ToString() || textBox1.Text == dataSet.Tables[0].Rows[i][1].ToString())
                    {
                        if (textBox2.Text == dataSet.Tables[0].Rows[i][2].ToString())
                        {
                            FIO = dataSet.Tables[0].Rows[i][3].ToString() + "\n" + dataSet.Tables[0].Rows[i][4].ToString() + "\n" + dataSet.Tables[0].Rows[i][5].ToString();
                            CheckedSignInRiel = true;
                            break;
                        }
                    }
                }
                sqlDataAdapter = new SqlDataAdapter("select Номер_телефона, Email, Пароль, Фамилия, Имя, Отчество from Клиенты where Статус_удаления = 'false'", sqlConnection);
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    if (textBox1.Text == dataSet.Tables[0].Rows[i][0].ToString() || textBox1.Text == dataSet.Tables[0].Rows[i][1].ToString())
                    {
                        if (textBox2.Text == dataSet.Tables[0].Rows[i][2].ToString())
                        {
                            FIO = dataSet.Tables[0].Rows[i][3].ToString() + "\n" + dataSet.Tables[0].Rows[i][4].ToString() + "\n" + dataSet.Tables[0].Rows[i][5].ToString();
                            CheckedSignInClient = true;
                            break;
                        }
                    }
                }
                if (CheckedSignInRiel == true)
                {
                    MessageBox.Show("Вы успешно авторизовались!");
                    string name = "Риэлтор";
                    account = new Account(name, FIO);
                    this.Hide();
                    account.Show();
                }
                else
                {
                    if (CheckedSignInClient == true)
                    {
                        MessageBox.Show("Вы успешно авторизовались!");
                        string name = "Клиент";
                        account = new Account(name, FIO);
                        this.Hide();
                        account.Show();
                    }
                    else
                    {
                        MessageBox.Show("Вы неправильно ввели телефон(Email) или пароль!", "Сообщение",MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                
            }
        }
        Regisration regisration;
        private void button2_Click(object sender, EventArgs e)
        {
            regisration = new Regisration();
            this.Hide();
            regisration.Show();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                textBox2.UseSystemPasswordChar = false;
            }
            if (checkBox1.Checked == false)
            {
                textBox2.UseSystemPasswordChar = true;
            }
        }
    }
}
