﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace PracticalProject
{
    public partial class Account : Form
    {
        string Role;
        string Phone, Email, Id_Riel, Id_Client;
        public Account(string role, string fio)
        {
            InitializeComponent();
            label1.Text = fio;
            Role = role;
            if (role == "Риэлтор")
            {
                label8.Visible = true;
                numericUpDown1.Visible = true;
                using (SqlConnection sqlConnection = new SqlConnection(connect))
                {
                    sqlDataAdapter = new SqlDataAdapter("select Фамилия, Имя, Отчество, Доля_сделки, Номер_телефона, Email, Пароль, Код_риэлтора from Риэлторы", sqlConnection);
                    dataSet = new DataSet();
                    sqlDataAdapter.Fill(dataSet);
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        if (fio == dataSet.Tables[0].Rows[i][0].ToString() + "\n" + dataSet.Tables[0].Rows[i][1].ToString() + "\n" + dataSet.Tables[0].Rows[i][2].ToString())
                        {
                            textBox1.Text = dataSet.Tables[0].Rows[i][0].ToString();
                            textBox2.Text = dataSet.Tables[0].Rows[i][1].ToString();
                            textBox3.Text = dataSet.Tables[0].Rows[i][2].ToString();
                            numericUpDown1.Value = Convert.ToInt32(dataSet.Tables[0].Rows[i][3]);
                            textBox4.Text = dataSet.Tables[0].Rows[i][4].ToString();
                            
                            if (textBox5.Text == "" && comboBox1.Text == "")
                            {
                                for (int j = 0; j < dataSet.Tables[0].Rows[i][5].ToString().Length; j++)
                                {
                                    textBox5.Text = dataSet.Tables[0].Rows[i][5].ToString().Substring(0, dataSet.Tables[0].Rows[i][5].ToString().IndexOf('@'));
                                }
                                comboBox1.Text = dataSet.Tables[0].Rows[i][5].ToString().Remove(0, dataSet.Tables[0].Rows[i][5].ToString().IndexOf('@')) + "";
                                comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
                            }
                            else
                            {
                                comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
                                textBox5.Text = dataSet.Tables[0].Rows[i][5].ToString();
                            }
                            Id_Riel = dataSet.Tables[0].Rows[i][7].ToString();
                            break;
                        }
                    }
                }
            }
            if (role == "Клиент")
            {
                using (SqlConnection sqlConnection = new SqlConnection(connect))
                {
                    sqlDataAdapter = new SqlDataAdapter("select Фамилия, Имя, Отчество, Номер_телефона, Email, Пароль, Код_клиента from Клиенты", sqlConnection);
                    dataSet = new DataSet();
                    sqlDataAdapter.Fill(dataSet);
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {
                        if (fio == dataSet.Tables[0].Rows[i][0].ToString() + "\n" + dataSet.Tables[0].Rows[i][1].ToString() + "\n" + dataSet.Tables[0].Rows[i][2].ToString())
                        {
                            textBox1.Text = dataSet.Tables[0].Rows[i][0].ToString();
                            textBox2.Text = dataSet.Tables[0].Rows[i][1].ToString();
                            textBox3.Text = dataSet.Tables[0].Rows[i][2].ToString();
                            textBox4.Text = dataSet.Tables[0].Rows[i][3].ToString();
                            
                            if (textBox5.Text == "" && comboBox1.Text == "")
                            {
                                for (int j = 0; j < dataSet.Tables[0].Rows[i][4].ToString().Length; j++)
                                {
                                    textBox5.Text = dataSet.Tables[0].Rows[i][4].ToString().Substring(0, dataSet.Tables[0].Rows[i][4].ToString().IndexOf('@'));
                                }
                                comboBox1.Text = dataSet.Tables[0].Rows[i][4].ToString().Remove(0, dataSet.Tables[0].Rows[i][4].ToString().IndexOf('@'));
                                comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
                            }
                            else
                            {
                                comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
                                textBox5.Text = dataSet.Tables[0].Rows[i][4].ToString();
                            }
                            Id_Client = dataSet.Tables[0].Rows[i][6].ToString();
                            break;
                        }
                    }
                }
            }
            surname = textBox1.Text;
            name = textBox2.Text;
            lastname = textBox3.Text;
            phone = textBox4.Text;
            mail = textBox5.Text + comboBox1.Text;
            share = numericUpDown1.Value.ToString();
        }
        string connect = ConnectSQL.Connect();
        SqlDataAdapter sqlDataAdapter;
        DataSet dataSet;
        SqlCommand sqlCommand;

        private void button1_Click(object sender, EventArgs e)
        {
            tabPage1.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }
        string surname, name, lastname, phone, mail, share;
        private void button6_Click(object sender, EventArgs e)
        {
            using (SqlConnection sqlConnection = new SqlConnection(connect))
            {
                if (Role == "Риэлтор")
                {
                    if (textBox1.Text != surname || textBox2.Text != name || textBox3.Text != lastname || textBox4.Text != phone || textBox5.Text+comboBox1.Text != mail || numericUpDown1.Value.ToString() != "")
                    {
                        if (textBox1.Text != "")
                        {
                            //sqlCommand = new SqlCommand($"update Риэлторы set Фамилия = '{textBox1.Text}' set Имя = '{textBox2.Text}' set Отчество = '{textBox3.Text}' set Доля_сделки = {numericUpDown1.Value} set Номер_телефона = {textBox4.Text} set Email = {textBox5.Text + comboBox1.Text} where Код_риэлтора = '{Id_Riel}'", sqlConnection);
                            sqlCommand = new SqlCommand($"update Риэлторы set Фамилия = '{textBox1.Text}' where Код_риэлтора = '{Id_Riel}'", sqlConnection);

                        }
                        if (textBox2.Text != "")
                        {
                            sqlCommand = new SqlCommand($"update Риэлторы  set Имя = '{textBox2.Text}' where Код_риэлтора = '{Id_Riel}'", sqlConnection);
                        }
                        if (textBox3.Text != "")
                        {
                            sqlCommand = new SqlCommand($"update Риэлторы set Отчество = '{textBox3.Text}' where Код_риэлтора = '{Id_Riel}'", sqlConnection);
                        }
                        if (textBox4.Text != "")
                        {
                            sqlCommand = new SqlCommand($"update Риэлторы set Номер_телефона = {textBox4.Text} where Код_риэлтора = '{Id_Riel}'", sqlConnection);
                        }
                        if (textBox5.Text+comboBox1.Text != "")
                        {
                            sqlCommand = new SqlCommand($"update Риэлторы set Email = {textBox5.Text + comboBox1.Text} where Код_риэлтора = '{Id_Riel}'", sqlConnection);
                        }
                        if (numericUpDown1.Value.ToString() != "")
                        {
                            sqlCommand = new SqlCommand($"update Риэлторы set Доля_сделки = {numericUpDown1.Value} where Код_риэлтора = '{Id_Riel}'", sqlConnection);
                        }
                        try
                        {
                            
                            sqlCommand.ExecuteNonQuery();
                            MessageBox.Show("Данные обновлены!");
                            //autorization = new Autorization();
                            //this.Hide();
                            //autorization.Show();
                        }
                        catch(Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                    }
                    else
                    {
                        MessageBox.Show("Вы не произвели изменений!");
                    }
                }
                if (Role == "Клиент")
                {

                }
            }
            
            
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Phone = textBox4.Text;
            Email = textBox5.Text + comboBox1.Text;
            using (SqlConnection sqlConnection = new SqlConnection(connect))
            {
                if (Role == "Риэлтор")
                {
                    sqlCommand = new SqlCommand($"update Риэлторы set Статус_удаления = 'True' where Номер_телефона = '{Phone}' or Email = '{Email}'", sqlConnection);
                    sqlConnection.Open();
                    try
                    {
                        sqlCommand.ExecuteNonQuery();
                        MessageBox.Show("Пользователь удалён");
                        autorization = new Autorization();
                        this.Hide();
                        autorization.Show();
                    }catch
                    {
                        MessageBox.Show("Произошли непредвиденные обстоятельства!");
                    }
                    
                }
                if (Role == "Клиент")
                {
                    sqlCommand = new SqlCommand($"update Клиенты set Статус_удаления = 'True' where Номер_телефона = '{Phone}' or Email = '{Email}'", sqlConnection);
                    sqlConnection.Open();
                    try
                    {
                        sqlCommand.ExecuteNonQuery();
                        MessageBox.Show("Пользователь удалён");
                        autorization = new Autorization();
                        this.Hide();
                        autorization.Show();
                    }
                    catch
                    {
                        MessageBox.Show("Произошли непредвиденные обстоятельства!");
                    }

                }
            }
        }
        Autorization autorization;
        private void Account_FormClosed(object sender, FormClosedEventArgs e)
        {
            autorization = new Autorization();
            this.Hide();
            autorization.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            autorization = new Autorization();
            this.Hide();
            autorization.Show();
        }
    }
}
